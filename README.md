## Équipe :

- OUERDANE YANIS
- AYACHE Salim
- LARABI Albdelghani

## Liens GITHUB pour le rendu :

**BackOffice:** https://gitlab.com/clone-spotify/spotify-clone-backoffice

**Front:** https://gitlab.com/clone-spotify/spotify-clone-front

**Back:** https://gitlab.com/clone-spotify/spotify-clone-server

## Mise en Prod:

**BackOffice:** http://vps-eb999426.vps.ovh.net:1337

**Front:** http://vps-eb999426.vps.ovh.net:1338

**Back:** http://vps-eb999426.vps.ovh.net:5001/api
