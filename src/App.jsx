import axios from "axios";
import { RouterProvider, createBrowserRouter } from "react-router-dom";

import {
  RootScreen,
  DashboardScreen,
  TracksScreen,
  AlbumsScreen,
  ArtistsScreen,
  LoginScreen,
  RegisterScreen,
  NotFoundScreen,
} from "@/screens";
import { TooltipProvider } from "@/components/ui";

import { ThemeProvider } from "@/contexts/theme-provider";

// axios.defaults.baseURL = "http://34.16.132.98:5001";
axios.defaults.baseURL = "http://vps-eb999426.vps.ovh.net:5001/";

const router = createBrowserRouter([
  {
    path: "/",
    element: <RootScreen />,
    errorElement: <NotFoundScreen />,
    children: [
      {
        path: "/",
        element: <DashboardScreen />,
      },
      {
        path: "tracks",
        element: <TracksScreen />,
      },
      {
        path: "albums",
        Component: AlbumsScreen,
      },
      {
        path: "artists",
        Component: ArtistsScreen,
      },
    ],
  },
  {
    path: "/login",
    Component: LoginScreen,
  },
  {
    path: "/register",
    Component: RegisterScreen,
  },
  {
    path: "*",
    Component: NotFoundScreen,
  },
]);

function App() {
  return (
    <ThemeProvider defaultTheme="light" storageKey="vite-ui-theme">
      <TooltipProvider>
        <RouterProvider router={router} />
      </TooltipProvider>
    </ThemeProvider>
  );
}

export default App;
