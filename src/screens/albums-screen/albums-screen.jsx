import PropTypes from "prop-types";
import { PageLayout } from "@/components/layout";
import {
  Dialog,
  DialogTrigger,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogDescription,
  Button,
  useToast,
} from "@/components/ui";
import { PlusCircledIcon } from "@radix-ui/react-icons";

import { columns } from "./components/columns";
import { DataTable } from "./components/data-table";

import { AddAlbumForm } from "./components/add-album-form";
import { createContext, useEffect, useState } from "react";
import axios from "axios";
import { useLocation } from "react-router-dom";

export const AlbumsContext = createContext();

export function AlbumsScreen({ menuOpen }) {
  const { state } = useLocation();

  const { toast } = useToast();
  const [data, setData] = useState([]);
  const [open, setOpen] = useState(false);

  function fetchAllAlbums() {
    axios
      .get("/albums")
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.log(error);
        toast({
          title: "Couldn't fetch data",
          description: "Please try again later.",
          variant: "warn",
        });
      });
  }

  const fetchOneAlbum = (id) => {
    axios
      .get("/albums/album/" + id)
      .then((response) => {
        setData([response.data]);
      })
      .catch((error) => {
        console.log(error);
        toast({
          title: "Couldn't fetch data",
          description: "Please try again later.",
          variant: "warn",
        });
      });
  };

  useEffect(() => {
    if (state) {
      return fetchOneAlbum(state.id);
    }
    fetchAllAlbums();

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <AlbumsContext.Provider
      value={{ data, setData, fetchData: fetchAllAlbums }}
    >
      <PageLayout className="p-8" menuOpen={menuOpen}>
        <div className="flex space-between items-center mb-4">
          <div>
            <h1 className="text-2xl font-semibold tracking-tight">Albums</h1>
            <p className="text-sm text-muted-foreground">List of all Albums</p>
          </div>
          <div className="ml-auto">
            <Dialog open={open} onOpenChange={setOpen}>
              <DialogTrigger asChild>
                <Button className="rounded-lg">
                  <PlusCircledIcon className="mr-2 h-4 w-4" />
                  Add Album
                </Button>
              </DialogTrigger>
              <DialogContent className="sm:max-w-[425px]">
                <DialogHeader>
                  <DialogTitle>Add Album</DialogTitle>
                  <DialogDescription>
                    Add a new Album to your list
                  </DialogDescription>
                </DialogHeader>
                <AddAlbumForm setOpenDialog={setOpen} />
              </DialogContent>
            </Dialog>
          </div>
        </div>

        <DataTable columns={columns} data={data} />
      </PageLayout>
    </AlbumsContext.Provider>
  );
}

AlbumsScreen.propTypes = {
  menuOpen: PropTypes.bool,
};
