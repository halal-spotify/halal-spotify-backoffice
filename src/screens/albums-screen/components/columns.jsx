// import {
//   Checkbox,
//   Button,
//   Tooltip,
//   TooltipTrigger,
//   TooltipContent,
// } from "@/components/ui";

import { DataTableColumnHeader } from "./data-table-column-header";
import { DataTableRowActions } from "./data-table-row-actions";
// import { Trash2 } from "lucide-react";

// const deleteSelectedRows = (rows) => {
//   // TODO: Do this
//   console.log(
//     "delete",
//     rows.map((e) => e.original.id),
//   );
// };

export const columns = [
  // {
  //   id: "select",
  //   header: ({ table }) => (
  //     <Checkbox
  //       checked={
  //         table.getIsAllPageRowsSelected() ||
  //         (table.getIsSomePageRowsSelected() && "indeterminate")
  //       }
  //       onCheckedChange={(value) => table.toggleAllPageRowsSelected(!!value)}
  //       aria-label="Select all"
  //       className="translate-y-[2px]"
  //     />
  //   ),
  //   cell: ({ row }) => (
  //     <Checkbox
  //       checked={row.getIsSelected()}
  //       onCheckedChange={(value) => row.toggleSelected(!!value)}
  //       aria-label="Select row"
  //       className="translate-y-[2px]"
  //     />
  //   ),
  //   enableSorting: false,
  //   enableHiding: false,
  // },
  {
    accessorKey: "id",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="ID" />
    ),
    cell: ({ row }) => <div className="w-[80px]">{row.getValue("id")}</div>,
    enableSorting: false,
    enableHiding: false,
  },
  {
    accessorKey: "name",
    title: "Title",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="Title" />
    ),
    cell: ({ row }) => {
      return (
        <div className="flex space-x-2">
          <span className="max-w-[500px] truncate font-medium">
            {row.getValue("name")}
          </span>
        </div>
      );
    },
  },
  {
    accessorKey: "users",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="Artist" />
    ),
    title: "Artist(s)",
    cell: ({ row }) => {
      return (
        <div className="flex space-x-2">
          <span className="max-w-[500px] truncate font-medium">
            {row
              .getValue("users")
              .map((user) => user.name)
              .join(", ")}
          </span>
        </div>
      );
    },
  },
  {
    accessorKey: "medias",
    title: "N°# Tracks",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="N°# Tracks" />
    ),
    cell: ({ row }) => {
      return (
        <div className="flex space-x-2">
          <span className="max-w-[500px] truncate font-medium">
            {row.getValue("medias").length}
          </span>
        </div>
      );
    },
  },
  {
    accessorKey: "createdAt",
    title: "Created At",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="Created At" />
    ),
    cell: ({ row }) => {
      const date = new Date(row.getValue("createdAt"));
      return (
        <div className="flex space-x-2">
          <span className="max-w-[500px] truncate font-medium">
            {date.toLocaleString()}
          </span>
        </div>
      );
    },
  },
  {
    id: "actions",
    // header: ({ table }) => (
    //   <Tooltip>
    //     <TooltipTrigger asChild>
    //       <Button
    //         variant="ghost"
    //         className="flex h-8 w-8 p-0 data-[state=open]:bg-muted"
    //         disabled={table.getFilteredSelectedRowModel().rows.length < 1}
    //       >
    //         <Trash2
    //           className="h-4 w-4"
    //           onClick={() =>
    //             deleteSelectedRows(table.getFilteredSelectedRowModel().rows)
    //           }
    //         />
    //         <span className="sr-only">Delete selected rows</span>
    //       </Button>
    //     </TooltipTrigger>
    //     <TooltipContent>
    //       <p className="font-normal">Delete selected rows</p>
    //     </TooltipContent>
    //   </Tooltip>
    // ),
    cell: ({ row }) => <DataTableRowActions row={row} />,
  },
];
