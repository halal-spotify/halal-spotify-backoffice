import { useState } from "react";
import PropTypes from "prop-types";

import { DotsHorizontalIcon } from "@radix-ui/react-icons";

import {
  Button,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
  Dialog,
  DialogTrigger,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogDescription,
  useToast,
  AlertDialog,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogCancel,
  AlertDialogAction,
} from "@/components/ui";

import { EditAlbumForm } from "./edit-album-form";
import axios from "axios";

export function DataTableRowActions({ row }) {
  const task = row.original;
  const { toast } = useToast();

  const [open, setOpen] = useState(false);

  function deleteArtist() {
    axios
      .delete(`/auth/users/`, {
        data: {
          userIDs: [task.id],
        },
      })
      .then((response) => {
        toast({
          title: response.data.message,
          variant: "success",
        });
      })
      .catch((error) => {
        console.log(error);
        toast({
          title: "Couldn't delete the artist",
          description: "Please try again later.",
          variant: "destructive",
        });
      });
  }

  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <AlertDialog>
        <DropdownMenu>
          <DropdownMenuTrigger asChild>
            <Button
              variant="ghost"
              className="flex h-8 w-8 p-0 data-[state=open]:bg-muted"
            >
              <DotsHorizontalIcon className="h-4 w-4" />
              <span className="sr-only">Open menu</span>
            </Button>
          </DropdownMenuTrigger>
          <DropdownMenuContent align="end" className="w-[160px]">
            <DialogTrigger asChild>
              <DropdownMenuItem className="cursor-pointer">
                Edit
              </DropdownMenuItem>
            </DialogTrigger>
            <DropdownMenuSeparator />
            <DropdownMenuItem className="cursor-pointer">
              Delete
            </DropdownMenuItem>
          </DropdownMenuContent>
        </DropdownMenu>
        <AlertDialogContent>
          <AlertDialogHeader>
            <AlertDialogTitle>Are you absolutely sure?</AlertDialogTitle>
            <AlertDialogDescription>
              This action cannot be undone. This will permanently delete the
              media and remove the data from our servers.
            </AlertDialogDescription>
          </AlertDialogHeader>
          <AlertDialogFooter>
            <AlertDialogCancel>Cancel</AlertDialogCancel>
            <AlertDialogAction onClick={deleteArtist}>
              Continue
            </AlertDialogAction>
          </AlertDialogFooter>
        </AlertDialogContent>
        <DialogContent className="sm:max-w-[425px]">
          <DialogHeader>
            <DialogTitle>Edit album</DialogTitle>
            <DialogDescription>Edit album&apos;s information</DialogDescription>
          </DialogHeader>
          <EditAlbumForm albumID={task.id} setOpenDialog={setOpen} />
        </DialogContent>
      </AlertDialog>
    </Dialog>
  );
}
DataTableRowActions.propTypes = {
  row: PropTypes.object.isRequired,
};
