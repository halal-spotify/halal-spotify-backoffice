import { useContext, useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";

import { DotsHorizontalIcon } from "@radix-ui/react-icons";

import {
  Button,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
  Dialog,
  DialogTrigger,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogDescription,
  AlertDialog,
  AlertDialogTrigger,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogCancel,
  AlertDialogAction,
  useToast,
} from "@/components/ui";

// import { statuses } from "../data/data";
import { EditTrackForm } from "./edit-track-form";
import { TracksContext } from "../tracks-screen";

export function DataTableRowActions({ row }) {
  const task = row.original;
  const { toast } = useToast();
  const trackCtx = useContext(TracksContext);

  const [open, setOpen] = useState(false);

  function deleteMedia() {
    axios
      .delete(`/media/${task.id}`)
      .then((response) => {
        trackCtx.fetchData();
        toast({
          title: response.data.message,
          variant: "success",
        });
      })
      .catch((error) => {
        console.log(error);
        toast({
          title: "Couldn't delete media",
          description: "Please try again later.",
          variant: "destructive",
        });
      });
  }

  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <AlertDialog>
        <DropdownMenu>
          <DropdownMenuTrigger asChild>
            <Button
              variant="ghost"
              className="flex h-8 w-8 p-0 data-[state=open]:bg-muted"
            >
              <DotsHorizontalIcon className="h-4 w-4" />
              <span className="sr-only">Open menu</span>
            </Button>
          </DropdownMenuTrigger>
          <DropdownMenuContent align="end" className="w-[160px]">
            <DialogTrigger asChild>
              <DropdownMenuItem className="cursor-pointer">
                Edit
              </DropdownMenuItem>
            </DialogTrigger>
            <DropdownMenuSeparator />

            <AlertDialogTrigger asChild>
              <DropdownMenuItem className="cursor-pointer">
                Delete
              </DropdownMenuItem>
            </AlertDialogTrigger>
          </DropdownMenuContent>
        </DropdownMenu>
        <AlertDialogContent>
          <AlertDialogHeader>
            <AlertDialogTitle>Are you absolutely sure?</AlertDialogTitle>
            <AlertDialogDescription>
              This action cannot be undone. This will permanently delete the
              media and remove the data from our servers.
            </AlertDialogDescription>
          </AlertDialogHeader>
          <AlertDialogFooter>
            <AlertDialogCancel>Cancel</AlertDialogCancel>
            <AlertDialogAction onClick={deleteMedia}>
              Continue
            </AlertDialogAction>
          </AlertDialogFooter>
        </AlertDialogContent>
        <DialogContent className="sm:max-w-[425px]">
          <DialogHeader>
            <DialogTitle>Edit track</DialogTitle>
            <DialogDescription>Edit track&apos;s information</DialogDescription>
          </DialogHeader>
          <EditTrackForm mediaID={task.id} setOpenDialog={setOpen} />
        </DialogContent>
      </AlertDialog>
    </Dialog>
  );
}
DataTableRowActions.propTypes = {
  row: PropTypes.object.isRequired,
};
