import { DataTableColumnHeader } from "./data-table-column-header";
import { DataTableRowActions } from "./data-table-row-actions";

export const columns = [
  {
    accessorKey: "id",
    title: "ID",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="ID" />
    ),
    cell: ({ row }) => <div className="w-[80px]">{row.getValue("id")}</div>,
    enableSorting: false,
    enableHiding: false,
  },
  {
    accessorKey: "name",
    title: "Title",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="Title" />
    ),
    cell: ({ row }) => {
      return (
        <div className="flex space-x-2">
          <span className="max-w-[500px] truncate font-medium">
            {row.getValue("name")}
          </span>
        </div>
      );
    },
  },
  {
    accessorKey: "users",
    title: "Artist",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="Artist" />
    ),
    cell: ({ row }) => {
      return (
        <div className="flex space-x-2">
          <span className="max-w-[500px] truncate font-medium">
            {row
              .getValue("users")
              .map((user) => user.name)
              .join(", ")}
          </span>
        </div>
      );
    },
  },
  {
    accessorKey: "createdAt",
    title: "Created At",
    header: ({ column }) => (
      <DataTableColumnHeader column={column} title="Created At" />
    ),
    cell: ({ row }) => {
      const date = new Date(row.getValue("createdAt"));
      return (
        <div className="flex space-x-2">
          <span className="max-w-[500px] truncate font-medium">
            {date.toLocaleString()}
          </span>
        </div>
      );
    },
  },
  {
    id: "actions",
    title: "Actions",
    // header: ({ table }) => (
    //   // TODO: Add API call dialog
    //   <Tooltip>
    //     <TooltipTrigger asChild>
    //       <Button
    //         variant="ghost"
    //         className="flex h-8 w-8 p-0 data-[state=open]:bg-muted"
    //         disabled={table.getFilteredSelectedRowModel().rows.length < 1}
    //       >
    //         <Trash2
    //           className="h-4 w-4"
    //           onClick={() =>
    //             deleteSelectedRows(table.getFilteredSelectedRowModel().rows)
    //           }
    //         />
    //         <span className="sr-only">Delete selected rows</span>
    //       </Button>
    //     </TooltipTrigger>
    //     <TooltipContent>
    //       <p className="font-normal">Delete selected rows</p>
    //     </TooltipContent>
    //   </Tooltip>
    // ),
    cell: ({ row }) => <DataTableRowActions row={row} />,
  },
];
