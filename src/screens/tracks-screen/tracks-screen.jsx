import { createContext } from "react";
import PropTypes from "prop-types";
import { PageLayout } from "@/components/layout";
import {
  Dialog,
  DialogTrigger,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogDescription,
  Button,
  useToast,
} from "@/components/ui";
import { PlusCircledIcon } from "@radix-ui/react-icons";

import { columns } from "./components/columns";
import { DataTable } from "./components/data-table";

import { AddTrackForm } from "./components/add-track-form";
import { useEffect, useState } from "react";
import axios from "axios";
import { useLocation } from "react-router-dom";

export const TracksContext = createContext();

export function TracksScreen({ menuOpen }) {
  const { state } = useLocation();

  const { toast } = useToast();
  const [data, setData] = useState([]);
  const [open, setOpen] = useState(false);

  function fetchAllTracks() {
    axios
      .get("/media")
      .then((response) => {
        setData(response.data);
      })
      .catch((error) => {
        console.log(error);
        toast({
          title: "Couldn't fetch data",
          description: "Please try again later.",
          variant: "warn",
        });
      });
  }

  function fetchOneTrack(id) {
    axios
      .get("/media/single/" + id)
      .then((response) => {
        setData([response.data]);
      })
      .catch((error) => {
        console.log(error);
        toast({
          title: "Couldn't fetch data",
          description: "Please try again later.",
          variant: "warn",
        });
      });
  }

  useEffect(() => {
    if (state) return fetchOneTrack(state.id);
    fetchAllTracks();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <TracksContext.Provider
      value={{ data, setData, fetchData: fetchAllTracks }}
    >
      <PageLayout className="p-8" menuOpen={menuOpen}>
        <div className="flex space-between items-center mb-4">
          <div>
            <h1 className="text-2xl font-semibold tracking-tight">Tracks</h1>
            <p className="text-sm text-muted-foreground">List of all tracks</p>
          </div>
          <div className="ml-auto">
            <Dialog open={open} onOpenChange={setOpen}>
              <DialogTrigger asChild>
                <Button className="rounded-lg">
                  <PlusCircledIcon className="mr-2 h-4 w-4" />
                  Add track
                </Button>
              </DialogTrigger>
              <DialogContent className="sm:max-w-[425px]">
                <DialogHeader>
                  <DialogTitle>Add track</DialogTitle>
                  <DialogDescription>
                    Add a new track to your list
                  </DialogDescription>
                </DialogHeader>
                <AddTrackForm setOpenDialog={setOpen} />
              </DialogContent>
            </Dialog>
          </div>
        </div>

        <DataTable columns={columns} data={data} />
      </PageLayout>
    </TracksContext.Provider>
  );
}

TracksScreen.propTypes = {
  menuOpen: PropTypes.bool,
};
