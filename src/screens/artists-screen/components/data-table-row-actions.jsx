import { useContext, useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";

import { DotsHorizontalIcon } from "@radix-ui/react-icons";

import {
  Button,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuSeparator,
  DropdownMenuTrigger,
  Dialog,
  DialogTrigger,
  DialogContent,
  DialogHeader,
  DialogTitle,
  DialogDescription,
  AlertDialog,
  AlertDialogTrigger,
  AlertDialogContent,
  AlertDialogHeader,
  AlertDialogTitle,
  AlertDialogDescription,
  AlertDialogFooter,
  AlertDialogCancel,
  AlertDialogAction,
  useToast,
} from "@/components/ui";

import { EditArtistForm } from "./edit-artist-form";
import { ArtistsContext } from "../artists-screen";

export function DataTableRowActions({ row }) {
  const task = row.original;
  const { toast } = useToast();
  const artistCtx = useContext(ArtistsContext);

  const [open, setOpen] = useState(false);

  function deleteArtist() {
    axios
      .delete("/auth/users/", {
        data: { userIDs: [task.id] },
      })
      .then((response) => {
        artistCtx.fetchData();
        toast({
          title: response.data.message,
          variant: "success",
        });
      })
      .catch((error) => {
        console.log(error);
        toast({
          title: error.response.data.message,
          description: "Please try again.",
          variant: "destructive",
        });
      });
  }

  return (
    <Dialog open={open} onOpenChange={setOpen}>
      <AlertDialog>
        <DropdownMenu>
          <DropdownMenuTrigger asChild>
            <Button
              variant="ghost"
              className="flex h-8 w-8 p-0 data-[state=open]:bg-muted"
            >
              <DotsHorizontalIcon className="h-4 w-4" />
              <span className="sr-only">Open menu</span>
            </Button>
          </DropdownMenuTrigger>
          <DropdownMenuContent align="end" className="w-[160px]">
            <DialogTrigger asChild>
              <DropdownMenuItem className="cursor-pointer">
                Edit
              </DropdownMenuItem>
            </DialogTrigger>
            <DropdownMenuSeparator />

            <AlertDialogTrigger asChild>
              <DropdownMenuItem className="cursor-pointer">
                Delete
              </DropdownMenuItem>
            </AlertDialogTrigger>
          </DropdownMenuContent>
        </DropdownMenu>
        <AlertDialogContent>
          <AlertDialogHeader>
            <AlertDialogTitle>Are you absolutely sure?</AlertDialogTitle>
            <AlertDialogDescription>
              This action cannot be undone. This will permanently delete the
              artist and remove the data from our servers.
            </AlertDialogDescription>
          </AlertDialogHeader>
          <AlertDialogFooter>
            <AlertDialogCancel>Cancel</AlertDialogCancel>
            <AlertDialogAction onClick={deleteArtist}>
              Continue
            </AlertDialogAction>
          </AlertDialogFooter>
        </AlertDialogContent>
        <DialogContent className="sm:max-w-[425px]">
          <DialogHeader>
            <DialogTitle>Edit Artist</DialogTitle>
            <DialogDescription>
              Edit artist&apos;s information
            </DialogDescription>
          </DialogHeader>
          <EditArtistForm artistID={task.id} setOpenDialog={setOpen} />
        </DialogContent>
      </AlertDialog>
    </Dialog>
  );
}
DataTableRowActions.propTypes = {
  row: PropTypes.object.isRequired,
};
