import { useContext, useState } from "react";
import PropTypes from "prop-types";
import axios from "axios";
import {
  DialogFooter,
  DialogClose,
  Button,
  Label,
  Input,
  Icons,
  useToast,
} from "@/components/ui";
import { ArtistsContext } from "../artists-screen";

export function AddArtistForm({ setOpenDialog }) {
  const { toast } = useToast();
  const artitCtx = useContext(ArtistsContext);

  const [isLoading, setIsLoading] = useState(false);
  const [newArtistForm, setNewArtistForm] = useState({
    name: "",
    email: "",
    password: "",
    ConfirmPassword: "",
  });

  const handleChange = (event) => {
    setNewArtistForm({
      ...newArtistForm,
      [event.target.name]: event.target.value,
    });
  };

  function onSubmit(event) {
    event.preventDefault();
    setIsLoading(true);

    axios
      .post("/auth/register", newArtistForm)
      .then(function () {
        artitCtx.fetchData();
        toast({
          title: "Success! Add new artist",
          description: "Artist added successfully.",
          variant: "success",
        });
        setOpenDialog(false);
        setIsLoading(false);
      })
      .catch(function (error) {
        console.log(error);
        toast({
          title: "Error! Add new artist",
          description:
            "Something bad happened, artist was not added, please try again.",
          variant: "destructive",
        });
        setIsLoading(false);
      });
  }

  const disableUpload =
    newArtistForm.name === "" ||
    newArtistForm.email === "" ||
    newArtistForm.password === "" ||
    newArtistForm.ConfirmPassword === "" ||
    newArtistForm.password !== newArtistForm.ConfirmPassword;

  return (
    <form onSubmit={onSubmit}>
      <div className="grid gap-4 py-4">
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="name" className="text-right">
            Name
          </Label>
          <Input
            id="name"
            name="name"
            placeholder="Ex: Tupac Shakur"
            className="col-span-3"
            disabled={isLoading}
            value={newArtistForm.name}
            onChange={handleChange}
          />
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="email" className="text-right">
            Email
          </Label>
          <Input
            id="email"
            name="email"
            placeholder="place@holder.com"
            type="email"
            className="col-span-3"
            disabled={isLoading}
            value={newArtistForm.email}
            onChange={handleChange}
          />
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="password" className="text-right">
            Password
          </Label>
          <Input
            id="password"
            name="password"
            placeholder="***"
            type="password"
            className="col-span-3"
            disabled={isLoading}
            value={newArtistForm.password}
            onChange={handleChange}
          />
        </div>
        <div className="grid grid-cols-4 items-center gap-4">
          <Label htmlFor="ConfirmPassword" className="text-right">
            Confirm Password
          </Label>
          <Input
            id="ConfirmPassword"
            name="ConfirmPassword"
            placeholder="***"
            type="password"
            className="col-span-3"
            disabled={isLoading}
            value={newArtistForm.ConfirmPassword}
            onChange={handleChange}
          />
        </div>
      </div>
      <DialogFooter className="flex items-center justify-between space-x-2">
        <DialogClose asChild>
          <Button variant="ghost" type="reset">
            Cancel
          </Button>
        </DialogClose>
        <Button type="submit" disabled={isLoading || disableUpload}>
          {isLoading && <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />}
          Save artist
        </Button>
      </DialogFooter>
    </form>
  );
}

AddArtistForm.propTypes = {
  setOpenDialog: PropTypes.func.isRequired,
};
