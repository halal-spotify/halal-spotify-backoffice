import { createContext, useState } from "react";

import { Outlet } from "react-router-dom";

import { Toaster } from "@/components/ui";
import { Menu, Sidebar } from "@/components/layout";

export const MenuContext = createContext(null);

export function RootScreen() {
  const [menuOpen, setMenuOpen] = useState(false);

  return (
    <MenuContext.Provider value={{ menuOpen, setMenuOpen }}>
      <div className="h-screen">
        <Menu />
        <div className="bg-background border-t h-[calc(100%-60px)]">
          <div
            className={`grid h-full ${
              menuOpen ? "grid-cols-5" : "grid-cols-menu"
            }`}
          >
            <Sidebar className="block h-full" />
            <Outlet />
          </div>
        </div>
      </div>
      <Toaster />
    </MenuContext.Provider>
  );
}
