import { useEffect, useState } from "react";
import axios from "axios";

import { Headphones } from "lucide-react";
import { PageLayout } from "@/components/layout";
import {
  Card,
  CardContent,
  CardHeader,
  CardTitle,
  Icons,
} from "@/components/ui";

export function DashboardScreen() {
  const [tracksData, setTracksData] = useState(null);
  const [albumsData, setAlbumsData] = useState(null);
  const [listenData, setListenData] = useState(null);

  useEffect(() => {
    axios
      .get("/media/stats")
      .then((response) => {
        setTracksData(response.data);
      })
      .catch((error) => {
        setTracksData(false);
        console.log(error);
      });
    axios
      .get("/albums/stats")
      .then((response) => {
        setAlbumsData(response.data);
      })
      .catch((error) => {
        setAlbumsData(false);
        console.log(error);
      });
    axios
      .get("/media/listenStats")
      .then((response) => {
        setListenData(response.data);
      })
      .catch((error) => {
        setListenData(false);
        console.log(error);
      });

    return () => {
      setTracksData(null);
      setAlbumsData(null);
      setListenData(null);
    };
  }, []);

  return (
    <PageLayout className="p-8">
      <div className="mb-4">
        <h1 className="text-2xl font-semibold tracking-tight">Dashboard</h1>
        <p className="text-sm text-muted-foreground">Overview & analytics</p>
      </div>

      <div className="grid gap-4 md:grid-cols-3 lg:grid-cols-3">
        <Card>
          <CardHeader className="flex flex-row items-center justify-between space-y-0 pb-2">
            <CardTitle className="text-sm font-medium">Total tracks</CardTitle>
            <Icons.song />
          </CardHeader>
          <CardContent>
            {tracksData === null ? (
              <div className="w-full grid place-items-center">
                <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />
              </div>
            ) : tracksData === false ? (
              <>
                <div className="w-full grid place-items-center text-xs text-muted-foreground">
                  Couldn&apos;t fetch data, please try again later
                </div>
              </>
            ) : (
              <>
                <div className="text-2xl font-bold">
                  {tracksData.totalMedias}
                </div>
                <p className="text-xs text-muted-foreground">
                  {tracksData.percentageAddedMedias} added this month
                </p>
              </>
            )}
          </CardContent>
        </Card>
        <Card>
          <CardHeader className="flex flex-row items-center justify-between space-y-0 pb-2">
            <CardTitle className="text-sm font-medium">Total albums</CardTitle>
            <Icons.album />
          </CardHeader>
          <CardContent>
            {albumsData === null ? (
              <div className="w-full grid place-items-center">
                <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />
              </div>
            ) : albumsData === false ? (
              <>
                <div className="w-full grid place-items-center text-xs text-muted-foreground">
                  Couldn&apos;t fetch data, please try again later
                </div>
              </>
            ) : (
              <>
                <div className="text-2xl font-bold">
                  {albumsData.totalAlbums}
                </div>
                <p className="text-xs text-muted-foreground">
                  {albumsData.percentageAddedAlbums} added this month
                </p>
              </>
            )}
          </CardContent>
        </Card>
        <Card>
          <CardHeader className="flex flex-row items-center justify-between space-y-0 pb-2">
            <CardTitle className="text-sm font-medium">Total listens</CardTitle>
            <Headphones size={16} />
          </CardHeader>
          <CardContent>
            {listenData === null ? (
              <div className="w-full grid place-items-center">
                <Icons.spinner className="mr-2 h-4 w-4 animate-spin" />
              </div>
            ) : listenData === false ? (
              <>
                <div className="w-full grid place-items-center text-xs text-muted-foreground">
                  Couldn&apos;t fetch data, please try again later
                </div>
              </>
            ) : (
              <>
                <div className="text-2xl font-bold">
                  {listenData.totalListens}
                </div>
                <p className="text-xs text-muted-foreground">
                  {listenData.percentageTotalListens} listens this month
                </p>
              </>
            )}
          </CardContent>
        </Card>
      </div>
    </PageLayout>
  );
}
