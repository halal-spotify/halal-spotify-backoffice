import PropTypes from "prop-types";

import { cn } from "@/utils";
import { buttonVariants } from "@/components/ui";

export function AuthLayout({ children, page }) {
  return (
    <>
      <div className="container relative h-screen flex-col items-center justify-center grid ">
        <a
          href={page.link}
          className={cn(
            buttonVariants({ variant: "ghost" }),
            "z-10 absolute right-4 top-4 md:right-8 md:top-8",
          )}
        >
          <p>{page.title}</p>
        </a>
        <div className="absolute right-4 top-4 md:left-8 md:top-8 flex items-center text-lg font-medium">
          Halal Spotify
        </div>
        {children}
      </div>
    </>
  );
}

AuthLayout.propTypes = {
  children: PropTypes.node.isRequired,
  page: PropTypes.shape({
    link: PropTypes.string.isRequired,
    title: PropTypes.string.isRequired,
  }).isRequired,
};
