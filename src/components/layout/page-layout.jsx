import PropTypes from "prop-types";
import { useContext } from "react";
import { MenuContext } from "../../screens/root-screen";
export function PageLayout({ children, className }) {
  const { menuOpen } = useContext(MenuContext);

  return (
    <div
      className={`${
        menuOpen ? "col-span-4 w-full" : "col-span-1"
      } border-l ${className}`}
    >
      {children}
    </div>
  );
}

PageLayout.propTypes = {
  children: PropTypes.node,
  className: PropTypes.string,
};
