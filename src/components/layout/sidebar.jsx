import PropTypes from "prop-types";
import { cn } from "@/utils";
import { Button } from "@/components/ui";
import { Link } from "react-router-dom";
import { useContext } from "react";
import { MenuContext } from "../../screens/root-screen";

const menuList = [
  {
    title: "Dashboard",
    url: "/",
    icon: (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        fill="none"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
        className="h-4 w-4"
      >
        <rect width="7" height="7" x="3" y="3" rx="1" />
        <rect width="7" height="7" x="14" y="3" rx="1" />
        <rect width="7" height="7" x="14" y="14" rx="1" />
        <rect width="7" height="7" x="3" y="14" rx="1" />
      </svg>
    ),
  },
  {
    title: "Tracks",
    url: "/tracks",
    icon: (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        fill="none"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
        className="h-4 w-4"
      >
        <circle cx="8" cy="18" r="4" />
        <path d="M12 18V2l7 4" />
      </svg>
    ),
  },
  {
    title: "Albums",
    url: "/albums",
    icon: (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        fill="none"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
        className="h-4 w-4"
      >
        <path d="m16 6 4 14" />
        <path d="M12 6v14" />
        <path d="M8 8v12" />
        <path d="M4 4v16" />
      </svg>
    ),
  },
  {
    title: "Artists",
    url: "/artists",
    icon: (
      <svg
        xmlns="http://www.w3.org/2000/svg"
        viewBox="0 0 24 24"
        fill="none"
        stroke="currentColor"
        strokeWidth="2"
        strokeLinecap="round"
        strokeLinejoin="round"
        className="h-4 w-4"
      >
        <path d="M19 21v-2a4 4 0 0 0-4-4H9a4 4 0 0 0-4 4v2" />
        <circle cx="12" cy="7" r="4" />
      </svg>
    ),
  },
];

export function Sidebar({ className }) {
  const { menuOpen } = useContext(MenuContext);

  return (
    <div className={cn(`pb-12 ${!menuOpen && "w-16"}`, className)}>
      <div className="space-y-4 py-4">
        <div className="px-3 py-2">
          {menuOpen && (
            <h2 className="mb-2 px-4 text-lg font-semibold tracking-tight">
              Menu
            </h2>
          )}
          <div className="space-y-1">
            {menuList.map((item, index) => (
              <Link key={index} to={item.url}>
                <Button
                  variant="ghost" // secondary for selected
                  className={`${
                    menuOpen ? "" : "py-4 px-3"
                  } w-full justify-start`}
                >
                  {item.icon}
                  {menuOpen && <span className="pl-2">{item.title}</span>}
                </Button>
              </Link>
            ))}
          </div>
        </div>
      </div>
    </div>
  );
}

Sidebar.propTypes = {
  className: PropTypes.string,
};
