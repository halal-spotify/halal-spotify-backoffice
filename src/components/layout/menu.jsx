import { MenuIcon, Search, UserCircle2 } from "lucide-react";
import { useMediaQuery } from "@/hooks/useMediaQuery";
import { useContext, useEffect, useState } from "react";
import {
  Input,
  Button,
  DropdownMenu,
  DropdownMenuContent,
  DropdownMenuItem,
  DropdownMenuTrigger,
  Popover,
  PopoverTrigger,
  PopoverContent,
  Command,
  CommandEmpty,
  CommandInput,
  CommandGroup,
  CommandItem,
  CommandList,
} from "@/components/ui";
import { MenuContext } from "@/screens";
import { useTheme } from "@/contexts/theme-provider";
import { Moon, Sun } from "lucide-react";
import { Link, useNavigate } from "react-router-dom";
import axios from "axios";

export function Menu() {
  const { menuOpen, setMenuOpen } = useContext(MenuContext);
  const isLessThenMd = useMediaQuery("md");

  useEffect(() => {
    if (!isLessThenMd) {
      setMenuOpen(false);
    }
  }, [isLessThenMd, setMenuOpen]);

  const toggleMenu = () => {
    if (isLessThenMd) setMenuOpen((prev) => !prev);
  };

  return (
    <div className="grid grid-cols-2 md:grid-cols-3 lg:grid-cols-5 items-center">
      {menuOpen ? (
        <div className="flex justify-between items-center m-4 pl-3">
          <Link to="/">
            <h1 className="text-lg font-bold tracking-tight">Halal Spotify</h1>
          </Link>
          <MenuIcon className="cursor-pointer" onClick={toggleMenu} />
        </div>
      ) : (
        <div className="flex gap-4 items-center m-4 pl-1">
          <MenuIcon className="cursor-pointer" onClick={toggleMenu} />
          <Link to="/">
            <h1 className="text-lg font-bold tracking-tight">Halal Spotify</h1>
          </Link>
        </div>
      )}
      <SearchBar />
      <div className="flex items-center gap-3 justify-self-end mr-4 pr-1">
        <div className="flex gap-1.5">
          <ThemeToggle />
          <UserMenu />
        </div>
      </div>
      <div />
    </div>
  );
}

export function ThemeToggle() {
  const { setTheme } = useTheme();

  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <Button variant="ghost" size="icon">
          <Sun className="h-[1.2rem] w-[1.2rem] rotate-0 scale-100 transition-all dark:-rotate-90 dark:scale-0" />
          <Moon className="absolute h-[1.2rem] w-[1.2rem] rotate-90 scale-0 transition-all dark:rotate-0 dark:scale-100" />
          <span className="sr-only">Toggle theme</span>
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent align="end">
        <DropdownMenuItem
          className="cursor-pointer"
          onClick={() => setTheme("light")}
        >
          Light
        </DropdownMenuItem>
        <DropdownMenuItem
          className="cursor-pointer"
          onClick={() => setTheme("dark")}
        >
          Dark
        </DropdownMenuItem>
        <DropdownMenuItem
          className="cursor-pointer"
          onClick={() => setTheme("system")}
        >
          System
        </DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  );
}

export function UserMenu() {
  return (
    <DropdownMenu>
      <DropdownMenuTrigger asChild>
        <Button variant="ghost" size="icon">
          <UserCircle2 className="cursor-pointer" />
          <span className="sr-only">User menu</span>
        </Button>
      </DropdownMenuTrigger>
      <DropdownMenuContent align="end">
        <DropdownMenuItem className="cursor-pointer">Profil</DropdownMenuItem>
        <DropdownMenuItem className="cursor-pointer">Log out</DropdownMenuItem>
      </DropdownMenuContent>
    </DropdownMenu>
  );
}

export function SearchBar() {
  const navigate = useNavigate();

  const [open, setOpen] = useState(false);
  const [value, setValue] = useState("");

  const [albums, setAlbums] = useState([]);
  const [artists, setArtists] = useState([]);
  const [songs, setSongs] = useState([]);

  useEffect(() => {
    function fetchData() {
      axios.get("/albums/search/" + value).then((response) => {
        setAlbums(response.data);
      });
      axios.get("/media/search/" + value).then((response) => {
        setSongs(response.data);
      });
      axios.get("/auth/search/" + value).then((response) => {
        setArtists(response.data);
      });
    }

    if (value.length > 3) {
      setTimeout(fetchData, 500);
    }
  }, [value]);

  return (
    <Popover open={open} onOpenChange={setOpen}>
      <PopoverTrigger asChild>
        <div className="hidden w-full sm:w-2/3 items-center space-x-2 justify-self-center md:flex md:col-span-1 lg:col-span-3">
          <Input
            className="border-r-0 rounded-lg rounded-e-none "
            type="text"
            placeholder="Search..."
            value={value}
            onChange={(e) => setValue(e.target.value)}
          />
          <Button
            style={{ margin: 0 }}
            className="bg-transparent border rounded-lg border-l-0 rounded-s-none hover:bg-transparent"
            type="submit"
          >
            <Search className="text-slate-400" size={18} />
          </Button>
        </div>
      </PopoverTrigger>
      <PopoverContent className="w-full p-0">
        <Command>
          <CommandInput
            value={value}
            onValueChange={setValue}
            placeholder="Search presets..."
          />

          <CommandList>
            <CommandEmpty>No result found.</CommandEmpty>
            <CommandGroup heading="Albums">
              {albums.map((preset) => (
                <CommandItem
                  key={preset.id}
                  onSelect={() => {
                    navigate("/albums", { state: preset });
                    setOpen(false);
                  }}
                >
                  {preset.name}
                </CommandItem>
              ))}
            </CommandGroup>
            <CommandGroup heading="Songs">
              {songs.map((preset) => (
                <CommandItem
                  key={preset.id}
                  onSelect={() => {
                    navigate("/tracks", { state: preset });
                    setOpen(false);
                  }}
                >
                  {preset.name}
                </CommandItem>
              ))}
            </CommandGroup>
            <CommandGroup heading="Artists">
              {artists.map((preset) => (
                <CommandItem
                  key={preset.id}
                  onSelect={() => {
                    navigate("/artists", { state: preset });
                    setOpen(false);
                  }}
                >
                  {preset.name}
                </CommandItem>
              ))}
            </CommandGroup>
          </CommandList>
        </Command>
      </PopoverContent>
    </Popover>
  );
}
